This GPL 3.0 licensed software is a modified copy of:
[https://gitlab.com/oeffi/oeffi]()

License conditions can be reviewed here: [https://www.gnu.org/licenses/gpl-3.0.txt]()

Copyright (c) 2010-2019 The Offi Developers

Modifications Copyright (c) 2019 Rucksack Mobile App Development

For detailed modification description see [https://gitlab.com/rucksack.dev/offi-plus]()